@extends('Frontend.Layouts.app_2')
@section('content')
    <div class="col-sm-8 setup">
        <div class="card-body">
            <h5 class="card-title">Thông tin yêu cầu dịch vụ</h5>
            <form method="POST">
                @csrf
                <div class="panel-body">
                    @include('message.errors')
                    @include('message.notification')
                    <div class="form-group">
                        <label class="text">Địa chỉ:</label>
                        <input type="text" class="form-control" name="address" id="address" />
                    </div>
                    <div class="form-group">
                        <label class="text">Ghi chú:</label>
                        <input type="text" class="form-control" name="content" id="content" />
                    </div>
                    <div class="form-group">
                        <label class="text">Loại xe:</label>
                        <select class="custom-select" name="vehicle_id">
                            @foreach ($vehicle as $item)
                                <?php $item->nameVehicle == 'ô tô'?  $vehicle_id = $item->id : ''; ?>
                                <option value="{{ $item->id }}">{{ $item->nameVehicle }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="text">Chọn Dịch vụ:</label>
                        <div class="form-group item-service">
                            @foreach ($service as $item)
                                @if ($item->vehicle_id == $vehicle_id)
                                    <div class="form-group">
                                        <input type="radio" name="service_id" value="{{ $item->id }}" />
                                        <label for="male">{{ $item->serviceName }}</label>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group-click">
                        <button type="submit" class="btn btn-primary">
                            Gửi yêu cầu
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".custom-select").change(function (e) { 
            e.preventDefault();
            var vehicle_id = $(this).val();
            $.ajax({
                type: "post",
                url: "{{asset('/serviceAjax')}}",
                data: {id: vehicle_id},
                success: function (response) {
                    var serviceArray = response, html = "";
                    serviceArray.map(function(value, key){
                        html += 
                            '<div class="form-group">' +
                                '<input type="radio" name="service_id" value="'+ value.id +'" />' +
                                '<label for="male">'+ value.serviceName +'</label>' +
                            '</div>';
                    });
                    $(".item-service").find(".form-group").remove();
                    // $(".item-service").append(html);  the first way
                    document.querySelector('.item-service').innerHTML = html; //the second way
                }
            });
        });
    });
</script>
@endsection
    