@extends('Frontend.Layouts.app_2')
@section('content')
    <div class="col-sm-9">
        <div class="card-body col-sm-12">
            <h5 class="card-title">Chi tiết hóa đơn</h5>
            <div class="mytable col-sm-12">
                <table class="table table-bordered table-hover">
                    @foreach ($invoice as $item)
                        @if (!empty($item['requirement']))
                        <thead class="thead-light">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Khách Hàng</th>
                                <th scope="col">Người Sửa</th>
                                <th scope="col">Dịch vụ</th>
                                <th scope="col">Trạng thái</th>
                                <th scope="col">Tổng chi phí</th>
                                @if ($item['requirement']->status == 0)
                                <th scope="col">Đánh giá dịch vụ</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">{{ $item->id }}</th>
                                @foreach ($partner as $ptn)
                                @if ($ptn->id == $item->partner_id)
                                <td>{{ $ptn->name }}</td>
                                @endif
                                @endforeach
                                <td>{{ Auth::user()->customer->name }}</td>
                                @foreach ($service as $sv)
                                @if ($sv->id == $item['requirement']->service_id)
                                <td>{{ $sv->serviceName }}</td>
                                @endif
                                @endforeach
                                <td>{{ $item['requirement']->status == 1 ? 'Chưa nhận' : ($item['requirement']->status == 2 ? 'Đã nhận đang sửa': 'Đã xong') }}</td>
                                <td>{{ $item->total }}</td>
                                @if ($item['requirement']->status == 0)
                                <td><span 
                                    data-toggle="modal"
                                    data-target="#my-modal" class="btn btn-primary">
                                    Đánh giá
                                    </span>
                                </td>
                                @endif
                            </tr>
                        </tbody>
                        @endif
                    @endforeach
                </table>
            </div>
            <div id="my-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="my-modal-title">Đánh giá dịch vụ</h4>
                            <button class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <span class="rate-np">0</span>
                            <div class="input-group-append">
                                <div class="rate">
                                    <div class="vote">
                                        <div class="star_1 ratings_stars"><input value="1" type="hidden"></div>
                                        <div class="star_2 ratings_stars"><input value="2" type="hidden"></div>
                                        <div class="star_3 ratings_stars"><input value="3" type="hidden"></div>
                                        <div class="star_4 ratings_stars"><input value="4" type="hidden"></div>
                                        <div class="star_5 ratings_stars"><input value="5" type="hidden"></div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="button" value="Đóng" class="btn btn-info" data-dismiss="modal">
                            <button type="submit" class="btn btn-primary" name="requirement_id" value="">Vote</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function () {
        $('.ratings_stars').hover(
	            // Handles the mouseover
	            function() {
	                $(this).prevAll().addBack().addClass('ratings_hover');
	                // $(this).nextAll().removeClass('ratings_vote'); 
	            },
	            function() {
	                $(this).prevAll().addBack().removeClass('ratings_hover');
	                // set_votes($(this).parent());
	            }
	        );

			$('.ratings_stars').click(function(){
				var Values =  $(this).find("input").val();
		        $(".rate-np").text(Values);
		    	if ($(this).hasClass('ratings_over')) {
		            $('.ratings_stars').removeClass('ratings_over');
		            $(this).prevAll().addBack().addClass('ratings_over');
		        } else {
		        	$(this).prevAll().addBack().addClass('ratings_over');
		        }
		    });
    });
    </script>
@endsection