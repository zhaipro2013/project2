@extends('Frontend.Layouts.app_2')
@section('content')
<div class="panel-primary col-sm-9">
    <div class="panel-heading">
        <h3 class="panel-title">Thông tin chi tiết</h3>
        <span class="pull-right clickable"><i class="fa fa-chevron-up" aria-hidden="true"></i></span>
    </div>
    <div class="panel panel-body table-responsive-md" style="display: none">
        @if (!empty($requirement))
        <table class="table table-bordered table-hover">
            <thead class="thead-light">
                <tr>
                    <th>Người sửa</th>
                    <th>Dịch vụ</th>
                    <th>loại xe</th>
                    <th>Nội dung</th>
                    <th>Ngày tạo</th>
                    <th>Giá tiền</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($requirement as $item)
                <tr>
                    @foreach ($partner as $ptn)
                        @if ($item['partner_id'] == $ptn->id)
                        <td id="{{$ptn->id}}">{{ $ptn->name }}</td>
                        @endif
                    @endforeach
                    @foreach ($service as $sv)
                        @if ($item['service_id'] == $sv->id)
                        <td>{{ $sv->serviceName }}</td>
                        <td>{{ $sv['vehicle']->nameVehicle }}</td>
                        @endif
                    @endforeach
                    <td>{{ $item['content'] }}</td>
                    <td>{{ \Carbon\Carbon::create($item['created_at'], 'Asia/Ho_Chi_Minh')->diffForHumans() }}</td>
                    <td name="{{$item['total']}}">{{ number_format($item['total'], 0, ',', '.') . "đ" }}</td>
                    <td>
                        <div class="btn-box">
                            <button type="button" class="btn btn-success" value="{{ $item['id'] }}">Chấp nhận</button>
                            <button class="btn btn-danger">Hủy</button>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <h4 class="text-info">Chưa tìm thấy người sửa !</h4>
        @endif
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(".btn-success").click(function (e) { 
            var id = $(this).closest('tr').find('td').eq(0).attr('id');
            var requirement = $(this).val();
            var price = $(this).closest('td').prev().attr('name');
            $.ajax({
                type: "post",
                url: "{{ route("user.invoice", ['id' => Auth::user()->customer_id]) }}",
                data: {partner_id: id, price: price, requirement_id: requirement},
                success: function (response) {
                    if(response){
                        location.reload();
                    }
                }
            });
        });
    });
</script>
@endsection
