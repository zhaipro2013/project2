@extends('Frontend.Layouts.app')
@section('content')
  <div class="col-sm-7 mr-sm-auto ml-sm-auto setup">
    <form method="POST">
      @csrf
        <div class="panel-primary col-sm-10 mr-sm-auto ml-sm-auto" style="margin-top: 0">
          <div class="panel-heading">
            <h2 class="text-center">Đăng nhập</h2>
          </div>
          <div class="panel panel-body">
            @include('message.errors')
            <div class="form-group">
              <label for="username">username</label>
              <input name="username" type="text" class="form-control" id="username">
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input name="password" type="password" class="form-control" id="password">
            </div>
            <button type="submit" class="btn btn-success col-12">Đăng nhập</button>
            <div class="block">
              <a href="{{ route("register") }}"> Đăng kí tài khoản</a>
              <a href="#"> Quên mật khẩu?</a>
            </div>
          </div>
        </div>
    </form>
  </div>
@endsection

