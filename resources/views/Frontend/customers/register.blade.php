@extends('Frontend.layouts.app')
@section('content')
  <div class="col-sm-7 mr-sm-auto ml-sm-auto setup">
    <div class="card-body">
      <h5 class="card-title">Đăng kí tài khoản</h5>
      <form class="signup-form" method="POST">
        @csrf
        <fieldset>
          @include('message.errors')
          @include('message.notification')
          <div class="form-group">
            <label for="name">Your name</label>
            <input name="name" type="text" class="form-control" id="name" />
          </div>
          <div class="form-group">
            <label for="username">Username</label>
            <input name="username" type="text" class="form-control" id="username" />
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input name="password" type="password" class="form-control" id="password" />
          </div>
          <div class="form-group">
            <label for="password_confirmation">Confirmation Password</label>
            <input name="password_confirmation" type="password" class="form-control" id="password_confirmation" />
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input name="email" type="email" class="form-control" id="email" />
          </div>
          <div class="form-group">
            <label for="phone_number">Phone number</label>
            <input name="phone_number" type="text" class="form-control" id="phone_number" />
          </div>
          <div class="form-group">
            <label for="address">Address</label>
            <input name="address" type="text" class="form-control" id="address" />
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="customRadio1" name="authority" class="custom-control-input" value="3" checked/>
            <label class="custom-control-label" for="customRadio1">Đăng kí khách hàng</label>
          </div>
          <div class="custom-control custom-radio">
            <input type="radio" id="customRadio2" name="authority" class="custom-control-input" value="2"/>
            <label class="custom-control-label" for="customRadio2">Đăng kí đối tác</label>
          </div>
          <button class="btn btn-success col-12" type="submit">Đăng ký</button>
        </fieldset>
      </form>
    </div>
  </div>
@endsection

