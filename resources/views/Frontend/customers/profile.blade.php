@extends('Frontend.Layouts.app')
@section('content')
<div class="now-navigation-profile">
    <div class="header-profile">
        <div class="row align-items-center">
            <div class="col-auto"></div>
            <div class="col txt-bold font15">{{ Auth::user()->customer->name }}</div>
        </div>
    </div>
    <div class="navigation-profile">
        <a class="item-navigation active" title="Cập nhật tài khoản" href="/account/profile">
            <div class="row">
                <div class="col-auto"><i class="fas fa-user"></i></div>
                <div class="col">Cập nhật tài khoản</div>
                <div class="col-auto"><i class="icon-arrow-thin right"></i></div>
            </div>
        </a>
        <div class="item-navigation">
            <a title="update_address" class="" href="{{ route("user.invoiceDetail", ['id' => Crypt::encrypt(Auth::user()->customer_id)]) }}">
                <div class="row">
                    <div class="col-auto"><i class="fas fa-shopping-cart"></i></div>
                    <div class="col">Thông tin đơn hàng</div>
                    <div class="col-auto">
                        <i class="icon-arrow-thin right"></i>
                    </div>
                </div>
            </a>
        </div>
    </div>
</div>
<div class="now-detail-profile">
    <div class="header-user-profile">Thông tin người dùng</div>
    <div class="content-user-profile">
        <div class="user-profile-update">
            <form method="POST">
                @csrf
                @include('message.errors')
                @include('message.notification')
                <div class="title-user">Thay đổi thông tin</div>
                <div class="row form-group align-items-center">
                    <div class="col-3 txt-bold">Tên</div>
                    <div class="col-4">
                        <div class="input-group">
                            <input name="name" placeholder="Tên" type="text" class="form-control"
                                value="{{ Auth::user()->customer->name }}" />
                        </div>
                    </div>
                </div>
                <div class="row form-group align-items-center">
                    <div class="col-3 txt-bold">Email</div>
                    <div class="col-8">
                        <div class="input-group">
                            <div class="show-email">{{ Auth::user()->email }}</div>
                        </div>
                    </div>
                </div>
                <div class="form-group verify-pass">
                    <div class="row align-items-center mar-bottom5">
                        <div class="col-3 txt-bold">Mật khẩu cũ</div>
                        <div class="col-4">
                            <div class="input-group validate-pass">
                                <input name="old_password" placeholder="Mật khẩu cũ" type="password"
                                    class="form-control" value="" />
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center mar-bottom5">
                        <div class="col-3 txt-bold">Mật khẩu mới</div>
                        <div class="col-4">
                            <div class="input-group validate-pass">
                                <input name="password" placeholder="Mật khẩu mới" type="password"
                                    class="form-control" value="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <button type="submit" class="btn btn-blue-4 btn-block">
                            Lưu thay đổi
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal fade" id="updatePrimaryPhoneConfirm" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <span class="close" data-dismiss="modal">x</span>
                    <div class="modal-header txt-center">
                        Cập nhật số điện thoại
                    </div>
                    <div class="modal-body">
                        Số điện thoại mới sẽ được liên kết với tài khoản của bạn và
                        tất cả các số điện thoại khác sẽ bị xóa. Vui lòng nhấn nút
                        Tiếp tục để nhập số điện thoại mà bạn muốn cập nhật.
                    </div>
                    <div class="modal-footer content-center">
                        <button type="button" class="btn btn-gray" data-dismiss="modal">
                            Không
                        </button>
                        <div>
                            <button class="btn btn-green" type="button" data-dismiss="modal">
                                Tiếp tục
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="user-profile-update">
            <div class="title-user">Quản lý số điện thoại</div>
            <div class="list-phone">
                <div class="list-phone-row">
                    <div class="row align-items-center">
                        <div class="col-3 txt-bold">{{ Auth::user()->customer->phone_number }}</div>
                        <div>
                            <span class="phone-verified"><i class="fas fa-check-circle txt-green font16"></i>Số
                                điện thoại đã được xác thực</span>
                        </div>
                        <div class="col-7 btn-update-phone-number">
                            <button class="btn btn-blue-4" type="button">
                                Cập nhật số điện thoại
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="font14">Bạn sẽ tiến hành cập nhật số điện thoại</div>
        </div>
    </div>
</div>
@endsection

 