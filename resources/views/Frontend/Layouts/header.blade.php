<header id="header">
    <div class="container">
      <div class="row">
        <div class="menu">
          <nav class="navbar navbar-expand-lg navbar-dark">
            <a class="navbar-brand" href="#">Sửa xe lưu động</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav">
                <li class="nav-item active">
                  <a class="nav-link" href="{{ route("user.home") }}">Trang chủ<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                  <a class="nav-link" href="{{route("user.service")}}">Dịch vụ<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active support">
                  <a class="nav-link" href="#">Hỗ trợ<span class="sr-only">(current)</span></a>
                  <div class="box-hotro-top">
                    <div class="item-hotro-wrapper">
                                        <div class="item-hotro">
                          <p>Hien_Laptop:</p>
                          <p class="ho-tro-phone">0702362766</p>
                          <a href="skype:vothinhuhien.ctm?chat">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-skype.png">
                          </a>
                          <a target="_blank" class="fbhotro" href="https://www.facebook.com/xuanvinhpc">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-facebook.png">
                          </a>
                          <div class="clr"></div>
                        </div>
                                        <div class="item-hotro">
                          <p>Thanh Tước_SalePC03:</p>
                          <p class="ho-tro-phone">0393607957</p>
                          <a href="skype:thanhtuoc002?chat">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-skype.png">
                          </a>
                          <a target="_blank" class="fbhotro" href="https://www.facebook.com/xuanvinhpc/">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-facebook.png">
                          </a>
                          <div class="clr"></div>
                        </div>
                                        <div class="item-hotro">
                          <p>Sanh_SalePC02:</p>
                          <p class="ho-tro-phone">0902868935</p>
                          <a href="skype:dongshan149?chat">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-skype.png">
                          </a>
                          <a target="_blank" class="fbhotro" href="">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-facebook.png">
                          </a>
                          <div class="clr"></div>
                        </div>
                                        <div class="item-hotro">
                          <p>Kim Phượng:</p>
                          <p class="ho-tro-phone">0914553222</p>
                          <a href="skype:?chat">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-skype.png">
                          </a>
                          <a target="_blank" class="fbhotro" href="">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-facebook.png">
                          </a>
                          <div class="clr"></div>
                        </div>
                                        <div class="item-hotro">
                          <p>Dũng_ CN Quy Nhơn:</p>
                          <p class="ho-tro-phone">0933881610</p>
                          <a href="skype:tridung.xuanvinh?chat">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-skype.png">
                          </a>
                          <a target="_blank" class="fbhotro" href="https://www.facebook.com/xuanvinhquynhon/">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-facebook.png">
                          </a>
                          <div class="clr"></div>
                        </div>
                                        <div class="item-hotro">
                          <p>Dung_CN Huế:</p>
                          <p class="ho-tro-phone">0944812121</p>
                          <a href="skype:Maidung.xuanvinh?chat">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-skype.png">
                          </a>
                          <a target="_blank" class="fbhotro" href="https://www.facebook.com/xuanvinhcomputerhue/">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-facebook.png">
                          </a>
                          <div class="clr"></div>
                        </div>
                                        <div class="item-hotro">
                          <p>Thuận_Dealer, Công nợ:</p>
                          <p class="ho-tro-phone">0944350980</p>
                          <a href="skype:thuan.xuanvinh?chat">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-skype.png">
                          </a>
                          <a target="_blank" class="fbhotro" href="https://www.facebook.com/xuanvinhpc/">
                            <img src="http://xuanvinh.vn/theme/zing-theme/images/icon-facebook.png">
                          </a>
                          <div class="clr"></div>
                        </div>
                                    </div>
                  </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                      data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Tài khoản
                  </a>
                @if (Auth::check())
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="{{ route('user-profile') }}">Quản lí tài khoản</a>
                  <a class="dropdown-item" href="{{ route("user.invoice", ['id' => Crypt::encrypt(Auth::user()->customer_id)]) }}">Lịch sử dịch vụ</a>
                  <a class="dropdown-item" href="#">Nhận xét của tôi</a>
                  <a class="dropdown-item" href="{{ route('logout') }}">Đăng xuất</a>
                </div>
                @else
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('login') }}">Đăng Nhập</a>
                    <a class="dropdown-item" href="{{ route('register') }}">Đăng kí</a>
                  </div>
                @endif
                </li>
              </ul>
              <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                <button class="btn btn-outline-info my-2 my-sm-0" type="submit">
                  Search
                </button>
              </form>
            </div>
          </nav>
        </div>
      </div>
    </div>
  </header>