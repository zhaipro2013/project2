<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Trang Chu</title>
    <link rel="stylesheet" href="{{asset('/Frontend/cssChitiet1.css')}}" />
    <link rel="stylesheet" href="{{asset('/Frontend/cssTrangchu.css')}}" />
    <link rel="stylesheet" href="{{asset('/Frontend/rate.css')}}" />
    <link rel="stylesheet" href="{{asset('/Frontend/bootstrap-4.5.3-dist/css/bootstrap.min.css')}}" />
    <script type="text/javascript" src="{{asset('/Frontend/bootstrap-4.5.3-dist/js/jquery-3.5.1.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
</head>
<body>
    <!-- ==================================================== -->

    @include('Frontend.layouts.header')

    <!-- ==================================================== -->
    
    <section id="slide" class="col-sm-12">
        <div class="container">
            <div class="row">
                <div id="mySlide" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#mySlide" data-slide-to="0" class="active"></li>
                        <li data-target="#mySlide" data-slide-to="1"></li>
                    </ul>

                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{ asset('/Frontend/image/55526788_2151013754980982_3634145998659387392_o.jpg') }}"
                                alt="" width="100%" height="320" />
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('/Frontend/image/55526788_2151013754980982_3634145998659387392_o.jpg') }}"
                                alt="" width="100%" height="320" />
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#mySlide" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#mySlide" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="row Content">
            @include('Frontend.layouts.leftSiderBar')

            @yield('content')
        </div>
    </div>

    @include('Frontend.layouts.footer')
    <!-- ==================================================== -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="{{asset('/Frontend/bootstrap-4.5.3-dist/js/bootstrap.min.js')}}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ready(function () {
        $(".panel-heading span.clickable").click(function (e) {
            if (!$(".panel-body").hasClass("show")) {
                $(".panel-body").show(200);
                $(this)
                    .find("i")
                    .removeClass("fa fa-chevron-up")
                    .addClass("fa fa-chevron-down");
                $(".panel-body").addClass("show");
            } else {
                $(".panel-body").hide(150);
                $(this)
                    .find("i")
                    .removeClass("fa fa-chevron-down")
                    .addClass("fa fa-chevron-up");
                $(".panel-body").removeClass("show");
            }
        });
        $(window).scroll(function () {
            var scrollTop = $(this).scrollTop();
            var flag = true;
            console.log($("#header").height());
            if (scrollTop > $("#header").height() && flag) {
                $("#header").addClass("scroll");
                flag = false;
            }
            if (scrollTop < 3) {
                $("#header").removeClass("scroll");
                flag = true;
            }
        });
    });
</script>
</body>
</html>