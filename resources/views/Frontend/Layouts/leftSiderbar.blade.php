<div class="col-3 setup">
    <div class="list-group">
        <span href="#" class="list-group-item list-group-item-dark">Danh mục</span>
        <a href="{{ route("user.service") }}" class="list-group-item">Yêu cầu dịch vụ</a>
        <a href="{{ route("user.invoice", ['id' => Crypt::encrypt(Auth::user()->customer_id)]) }}" class="list-group-item">Chi tiết dịch vụ</a>
        <a href="{{ route("user.invoiceDetail", ['id' => Crypt::encrypt(Auth::user()->customer_id)]) }}" class="list-group-item">Chi tiết hóa đơn</a>
        <a href="#" class="list-group-item">Thống kê</a>
        <a href="{{ route('user-profile') }}" class="list-group-item">Thông tin cá nhân</a>
    </div>
</div>