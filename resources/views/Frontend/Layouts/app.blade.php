<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Trang Chu</title>
    <link rel="stylesheet" href="{{asset('/Frontend/cssChitiet1.css')}}" />
    <link rel="stylesheet" href="{{asset('/Frontend/cssQLTK.css')}}" />
    <link rel="stylesheet" href="{{asset('/Frontend/cssTrangchu.css')}}" />
    <link rel="stylesheet" href="{{asset('/Frontend/bootstrap-4.5.3-dist/css/bootstrap.min.css')}}" />
    <script type="text/javascript" src="{{asset('/Frontend/bootstrap-4.5.3-dist/js/jquery-3.5.1.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
</head>
<body>
    <!-- ==================================================== -->

    @include('Frontend.layouts.header')

    <!-- ==================================================== -->
    
    <div class="container">
        <div class="row">

           @yield('content')
        </div>
    </div>

    @include('Frontend.layouts.footer')
    <!-- ==================================================== -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<script src="{{asset('/Frontend/bootstrap-4.5.3-dist/js/bootstrap.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".panel-heading span.clickable").click(function (e) {
            if (!$(".panel-body").hasClass("show")) {
                $(".panel-body").show(200);
                $(this)
                    .find("i")
                    .removeClass("fa fa-chevron-up")
                    .addClass("fa fa-chevron-down");
                $(".panel-body").addClass("show");
            } else {
                $(".panel-body").hide(150);
                $(this)
                    .find("i")
                    .removeClass("fa fa-chevron-down")
                    .addClass("fa fa-chevron-up");
                $(".panel-body").removeClass("show");
            }
        });
        $(window).scroll(function () {
            var scrollTop = $(this).scrollTop();
            var flag = true;
            console.log($("#header").height());
            if (scrollTop > $("#header").height() && flag) {
                $("#header").addClass("scroll");
                flag = false;
            }
            if (scrollTop < 3) {
                $("#header").removeClass("scroll");
                flag = true;
            }
        });
    });
</script>
</body>
</html>
