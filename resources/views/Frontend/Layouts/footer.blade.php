<footer class="page-footer font-small blue pt-4">
  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">
      <!-- Grid row -->
      <div class="row">
          <!-- Grid column -->
          <div class="col-md-6 mt-md-0">
              <!-- Content -->
              <h5 class="text-uppercase text-info">Thọ Béo sữa xe</h5>
              <p>SỜ TA ÚP bố đời HTML&amp;CSS cũng không biết GG 🤡</p>
          </div>
          <!-- Grid column -->

          <hr class="clearfix w-100 d-md-none pb-3">

          <!-- Grid column -->
          <div class="col-md-3 mb-md-0">
              <!-- Links -->
              <h5 class="text-uppercase text-info">My Team includes</h5>

              <ul class="list-unstyled">
                  <li>
                      <a href="#!">King Hề Thọ béo</a>
                  </li>
                  <li>
                      <a href="#!">useless Hề Dương tokuda</a>
                  </li>
                  <li>
                      <a href="#!">Silent Hề Kim</a>
                  </li>
                  <li>
                      <a href="#!">And Hải bezos</a>
                  </li>
              </ul>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-3 mb-md-0 mb-3">
              <!-- Links -->
              <h5 class="text-uppercase text-info">Inside out</h5>

              <ul class="list-unstyled">
                  <li>
                      <a href="#!">Love</a>
                  </li>
                  <li>
                      <a href="#!">Hate</a>
                  </li>
                  <li>
                      <a href="#!">Angry</a>
                  </li>
                  <li>
                      <a href="#!">Sad</a>
                  </li>
              </ul>
          </div>
          <!-- Grid column -->
      </div>
      <!-- Grid row -->
  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center">
<p><span>Cơ quan chủ quản: Công ty Cổ phần Quảng cáo Trực tuyến 24H Trụ sở: Tầng 12, Tòa nhà Geleximco, 36 Hoàng Cầu, Phường Ô Chợ Dừa, Quận Đống Đa, TP Hà Nội. Tel: (84-24) 73 00 24 24 hoặc (84-24) 3512 1806 - Fax: (84-24) 3512 1804. Chi nhánh: Tầng 7, Tòa nhà Việt Úc, 402 Nguyễn Thị Minh Khai, Phường 5, Quận 3, TP. Hồ Chí Minh. Tel: (84-28) 7300 2424 / Giấy phép số 332/GP – TT ĐT ngày cấp 22/01/2018 SỞ THÔNG TIN VÀ TRUYỀN THÔNG HÀ NỘI. Chịu trách nhiệm xuất bản: Phan Minh Tâm. HOTLINE: 0965 08 24 24</span>
<a href="https://mdbootstrap.com/" class="text-danger">Don't click me! Please</a>
</p>
  </div>
  <!-- Copyright -->
</footer>