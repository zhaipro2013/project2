@extends('Backend.layouts.index')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Chi tiết hóa đơn</h1>
	</div>
</div><!--/.row-->

<div class="row">
	<div class="col-xs-12 col-md-12 col-lg-12">
        <div class="panel panel-primary">
			<div class="panel-heading">Danh sách hóa đơn</div>
			<div class="panel-body">
                <div class="bootstrap-table">
                    <div class="table-responsive">
                        @if (!empty($invoice->toArray()))
                        <table class="table table-bordered" style="margin-top:20px;">
                            <thead>
								<tr class="bg-primary">
									<th>Tên khách</th>
									<th>Tên loại xe</th>
									<th>Dịch vụ</th>
									<th>Trạng thái</th>
									<th>Ngày khời tạo</th>
									<th>Giá tiền</th>
									<th>Xác nhận</th>
								</tr>
                            </thead>
                            <tbody>
                                @foreach ($invoice as $item)
                                <tr>
                                    @foreach ($customer as $cst)
                                        @if ($item['requirement']->customer_id == $cst->id)
                                        <td>{{ $cst->name }}</td>
                                        @endif
                                    @endforeach
                                    @foreach ($service as $sv)
                                        @if ($item['requirement']->service_id == $sv->id)
                                        <td>{{ $sv->serviceName }}</td>
                                        <td>{{ $sv['vehicle']->nameVehicle }}</td>
                                        @endif
                                    @endforeach
                                    <td>{{ $item['requirement']->status == 1 ? 'Chưa nhận' : ($item['requirement']->status == 2 ? 'Đã nhận đang sửa': 'Đã xong')}}</td>
                                    <td>{{ \Carbon\Carbon::create($item->created_at, 'Asia/Ho_Chi_Minh')->diffForHumans() }}</td>
                                    <td>{{ number_format($item->total, 0, ',', '.') . "đ" }}</td>
                                    <td>
                                        @if ($item['requirement']->status == 2)
                                        <form method="POST">@csrf<button onclick="return confirm('Xác nhận đã hoàn tất !')" type="submit" class="btn btn-success" name="requirement" value="{{ $item['requirement']->id }}">Xong</button></form> 
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                            <h4 class="text-info">Hiện không có đơn hàng nào !</h4>
                        @endif
                    </div>
                </div>
            </div>
		</div>
	</div>
</div><!--/.row-->
@endsection