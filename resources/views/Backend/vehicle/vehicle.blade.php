@extends('Backend.layouts.index')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Danh mục Loại xe</h1>
	</div>
</div><!--/.row-->

<div class="row">
	<div class="col-xs-12 col-md-5 col-lg-5">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Thêm danh Loại xe
				</div>
				<div class="panel-body">
					<form method="post">
						@csrf
						<div class="form-group">
							<label>Tên Loại xe:</label>
							<input type="text" name="nameVehicle" class="form-control" placeholder="Tên loại xe">
						</div>
						<input type="submit" value="Add" class="btn btn-success col-xs-12">
					</form>
				</div>
			</div>
	</div>
	<div class="col-xs-12 col-md-7 col-lg-7">
		<div class="panel panel-primary">
			<div class="panel-heading">Danh sách Loại xe</div>
			<div class="panel-body">
				<div class="bootstrap-table">
					<table class="table table-bordered">
						@if ($vehicleList->isNotEmpty())
							<thead>
								<tr class="bg-primary">
									<th>Tên Loại xe</th>
									<th style="width:50%">Tùy chọn</th>
								</tr>
							</thead>
							<tbody>
							@include('message.notification')
							@include('message.errors')
							@foreach ($vehicleList as $item)
								<tr>
									<td>{{$item->nameVehicle}}</td>
									<td>
										<a href="{{route('service.add', ['id'=>$item->id])}}" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> dịch vụ</a>
										<a href="#" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Sửa</a>
										<a href="{{route('vehicle.delete', ['id'=>$item->id])}}" onclick="return confirm('Bạn có chắc chắn muốn xóa?')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Xóa</a>
									</td>
								</tr>
							@endforeach
							</tbody>
						@endif
					</table>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div><!--/.row-->
@endsection

