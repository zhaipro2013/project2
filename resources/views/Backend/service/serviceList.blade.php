@extends('Backend.layouts.index')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Danh sách yêu cầu</h1>
	</div>
</div><!--/.row-->

<div class="row">
	<div class="col-xs-12 col-md-12 col-lg-12">
		
		<div class="panel panel-primary">
			<div class="panel-heading">Yêu cầu khách hàng</div>
			<div class="panel-body">
				@include('message.notification')
				@include('message.errors')
				<div class="bootstrap-table">
					<div class="table-responsive">
						<table class="table table-bordered" style="margin-top:20px;">				
							<thead>
								<tr class="bg-primary">
									<th>Tên dịch vụ</th>
									<th>Loại xe</th>
									<th>Địa chỉ</th>
									<th>Tên Khách hàng</th>
									<th>Ghi chú</th>
									<th>Trạng thái</th>
									<th>Time</th>
									<th>Báo giá</th>
								</tr>
							</thead>
							<tbody>
							@if (!empty($list->toArray()))
								@foreach ($list as $requirement)
									@if ($requirement->status == 1)
									<tr>
										@foreach ($services as $service)
											@if ($requirement->service_id == $service->id)
												<td>{{ $service->serviceName }}</td>
												<td>{{ $service['vehicle']->nameVehicle }}</td>
											@endif
										@endforeach
										<td>{{ $requirement->address }}</td>
										<td>{{ $requirement['customer']->name}}</td>
										<td>{{ $requirement->content == null ? '' : $requirement->content}}</td>
										<td>{{ $requirement->status == 1 ? 'Chưa nhận' : ($requirement->status == 2 ? 'Đã nhận đang sửa': 'Đã xong')}}</td>
										<td>{{ \Carbon\Carbon::create($requirement->created_at, 'Asia/Ho_Chi_Minh')->diffForHumans() }}</td>
										<td>
										@if ($requirement->status == 1)
											<button type="button" class="cost btn btn-primary" data-toggle="modal"
											data-target="#my-modal" name="{{ $requirement->id }}">
											Báo giá
											</button>
										@endif
										</td>
									</tr>
									@endif
								@endforeach
							@endif
							</tbody>
						</table>							
					</div>
					<div id="my-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="my-modal-title" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="my-modal-title">Báo giá dịch vụ</h5>
									<button class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<form method="post">
									@csrf
									<div class="modal-body">
										<input type="text" name="cost" class="form-control">
										<div class="input-group-append">
											<span class="input-group-text">VNĐ</span>
										</div>
									</div>
									<div class="modal-footer">
										<input type="button" value="Đóng" class="btn btn-info" data-dismiss="modal">
										<button type="submit" class="btn btn-primary" name="requirement_id" value="">Gửi</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div><!--/.row-->
<script type="text/javascript">
	$(document).ready(function () {
		$(".cost").click(function (e) { 
			e.preventDefault();
			$('.modal-footer button').val($(this).attr('name'));
		});
	});
</script>
@endsection
	