@extends('Backend.layouts.index')
@section('content')
<div class="row">
	<div class="col-lg-12">
		<h1 class="page-header">Danh mục dịch vụ</h1>
	</div>
</div><!--/.row-->

<div class="row">
	<div class="col-xs-12 col-md-5 col-lg-5">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Thêm dịch vụ
				</div>
				<div class="panel-body">
					<form method="post">
						@csrf
						<div class="form-group">
							<label>Tên dịch vụ:</label>
							<input type="text" name="serviceName" class="form-control" placeholder="Tên dịch vụ">
						</div>
						<input type="submit" value="Add" class="btn btn-success col-xs-12">
					</form>
				</div>
			</div>
	</div>
	<div class="col-xs-12 col-md-7 col-lg-7">
		<div class="panel panel-primary">
			<div class="panel-heading">Danh sách dịch vụ {{ $serviceList[0]['nameVehicle'] }}</div>
			<div class="panel-body">
				<div class="bootstrap-table">
					<table class="table table-bordered">
						@if (!empty($serviceList[0]['service']))
							<thead>
								<tr class="bg-primary">
									<th>Tên dịch vụ</th>
									<th style="width:30%">Tùy chọn</th>
								</tr>
							</thead>
							<tbody>
							@include('message.notification')
							@include('message.errors')
							@foreach ($serviceList[0]['service'] as $item)
								<tr>
									<td>{{$item['serviceName']}}</td>
									<td>
										<a href="#" class="btn btn-warning"><span class="glyphicon glyphicon-edit"></span> Sửa</a>
										<a href="{{route('service.delete', ['id'=>$item['id']])}}" onclick="return confirm('Bạn có chắc chắn muốn xóa?')" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> Xóa</a>
									</td>
								</tr>
							@endforeach
							</tbody>
						@endif
					</table>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div><!--/.row-->
<a href="{{route("vehicle.add")}}" class="btn btn-primary">Back</a>
@endsection

