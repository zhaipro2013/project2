<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Dashboard | Partner Admin</title>
<link href="{{asset('/Backend/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('/Backend/css/datepicker3.css')}}" rel="stylesheet">
<link href="{{asset('/Backend/css/styles.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{asset('/Backend/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('/Backend/js/lumino.glyphs.js')}}"></script>
<script type="text/javascript" src="{{asset('/Frontend/bootstrap-4.5.3-dist/js/jquery-3.5.1.min.js')}}"></script>
</head>
<body>
		@include('Backend.layouts.header')
		<!--/.header-->

		@include('Backend.layouts.leftSidebar')
		<!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">		
		@yield('content')
	</div>	<!--/.main-->
		  

	<script src="{{asset('/Backend/js/jquery-1.11.1.min.js')}}"></script>
	<script src="{{asset('/Backend/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('/Backend/js/chart.min.js')}}"></script>
	<script src="{{asset('/Backend/js/chart-data.js')}}"></script>
	<script src="{{asset('/Backend/js/easypiechart.js')}}"></script>
	<script src="{{asset('/Backend/js/easypiechart-data.js')}}"></script>
	<script src="{{asset('/Backend/js/bootstrap-datepicker.js')}}"></script>
	<script type="text/javascript">
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>
