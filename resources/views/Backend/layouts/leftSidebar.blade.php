<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <ul class="nav menu">
        <li role="presentation" class="divider"></li>
        <li class="active"><a href="{{route("home")}}"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Trang chủ</a></li>
        <li><a href="{{route("vehicle.add")}}"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg> Dich vụ</a></li>
        <li><a href="{{route("list")}}"><svg class="glyph stroked bag"><use xlink:href="#stroked-bag"></use></svg>Danh sách yêu cầu dịch vụ</a></li>
        <li><a href="{{route("invoice", ['id' => Auth::user()->partner_id])}}"><svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg> Chi tiết đơn hàng</a></li>
        <li role="presentation" class="divider"></li>
    </ul>  
</div><!--/.sidebar-->