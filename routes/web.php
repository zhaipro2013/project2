<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'Frontend'], function () {
    Route::group(['middleware' => ['memberNotLogin']], function () {
        Route::get('/', 'homeController@homeShow')->name('user.home');
        Route::get('/login', 'customerController@showLogin')->name('login');
        Route::post('/login', 'customerController@Login');
        Route::get('/register', 'customerController@showRegister')->name('register');
        Route::post('/register', 'customerController@Register');
    });
    Route::group(['middleware' => ['memberLogin:Customer']], function () {
        Route::get('/member-logout', 'customerController@logout')->name('user-logout');
        Route::get('/profile', 'customerController@showProfile')->name('user-profile');
        Route::post('/profile', 'customerController@updateProfile')->name('user-profile');
        Route::get('/service', 'serviceController@showServices')->name('user.service');
        Route::post('/service', 'serviceController@handleServices');
        Route::post('/serviceAjax', 'serviceController@handleServicesAjax');
        Route::get('/service-detail/cst{id}', 'serviceController@showInvoice')->name('user.invoice');
        Route::post('/service-detail/cst{id}', 'serviceController@handleInvoice')->name('user.invoice');
        Route::get('/invoice-detail/cst{id}', 'InvoiceController@showInvoiceDetail')->name('user.invoiceDetail');
    });
});

Auth::routes(['login' => false, 'register' => false]);

Route::group([
    'prefix' => 'Admin',
    'namespace' => 'Auth',
], function () {
    Route::get('/', 'LoginController@showLoginForm');
    Route::get('/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'LoginController@login');
    Route::get('/logout', 'LoginController@logout')->name('logout');
});

Route::group([
    'prefix' => 'Admin',
    'namespace' => 'Backend',
    'middleware' => ['Admin:Partner']
], function () {
    Route::get('/home', 'homeController@index')->name('home');
    Route::get('/add-vehicle', 'serviceController@showVehicle')->name('vehicle.add');
    Route::post('/add-vehicle', 'serviceController@addVehicle');
    Route::get('/delete-vehicle/{id}', 'serviceController@destroyVehicle')->name('vehicle.delete');
    Route::get('/add-service/{id}', 'serviceController@showService')->name('service.add');
    Route::post('/add-service/{id}', 'serviceController@addService');
    Route::get('/delete-service/{id}', 'serviceController@destroyService')->name('service.delete');
    Route::get('/requirement', 'serviceController@showlist')->name('list');
    Route::post('/requirement', 'serviceController@handleRequirement');
    Route::get('/invoice/{id}', 'InvoiceController@showInvoice')->name('invoice');
    Route::post('/invoice/{id}', 'InvoiceController@handleInvoice');
});


