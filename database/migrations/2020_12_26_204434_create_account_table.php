<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account', function (Blueprint $table) {
            $table->id();
            $table->string("username", 100);
            $table->string('password');
            $table->string('email')->unique();
            $table->integer('auth_id');
            $table->bigInteger('partner_id')->unsigned()->nullable();

            $table->foreign('partner_id')
                  ->references('id')->on('partners')
                  ->onDelete('cascade');
            $table->bigInteger('customer_id')->unsigned()->nullable();

            $table->foreign('customer_id')
                  ->references('id')->on('customers')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account');
    }
}
