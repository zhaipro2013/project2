<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice', function (Blueprint $table) {
            $table->id();
            $table->double('total');
            $table->string('serviceArise')->nullable();
            $table->double('costIncurred')->nullable();

            $table->bigInteger('partner_id')->unsigned();

            $table->foreign('partner_id')
                  ->references('id')->on('partners')
                  ->onDelete('cascade');

            $table->bigInteger('requirement_id')->unsigned();

            $table->foreign('requirement_id')
                  ->references('id')->on('requirement')
                  ->onDelete('cascade');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice');
    }
}
