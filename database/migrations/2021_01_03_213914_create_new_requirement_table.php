<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewRequirementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requirement', function (Blueprint $table) {
            $table->id();
            $table->string('content')->nullable();
            $table->integer('status')->default(1);
            $table->string('address', 100);
            $table->bigInteger('customer_id')->unsigned()->nullable();

            $table->foreign('customer_id')
                  ->references('id')->on('customers')
                  ->onDelete('cascade');
            
            $table->bigInteger('partner_id')->unsigned()->nullable();

            $table->foreign('partner_id')
                  ->references('id')->on('partners')
                  ->onDelete('cascade');
                  
            $table->bigInteger('vehicle_id')->unsigned();

            $table->foreign('vehicle_id')
                  ->references('id')->on('vehicle')
                  ->onDelete('cascade');

            $table->bigInteger('service_id')->unsigned();

            $table->foreign('service_id')
                  ->references('id')->on('service')
                  ->onDelete('cascade');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requirement');
    }
}
