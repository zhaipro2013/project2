<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Authority_level extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authority')->insert([
            ['auth_name' => 'Admin'],
            ['auth_name' => 'Partner'],
            ['auth_name' => 'Customer']
        ]);
    }
}
