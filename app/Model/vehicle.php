<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class vehicle extends Model
{
    use Notifiable;
    
    protected $table = 'vehicle';
    

    protected $fillable = [
        'nameVehicle'
    ];

    public function service()
    {
        return $this->hasMany('App\Model\service');
    }
}
