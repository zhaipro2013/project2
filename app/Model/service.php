<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class service extends Model
{
    protected $table = "service";

    protected $fillable = [
        'serviceName', 'vehicle_id'
    ];

    public function vehicle()
    {
        return $this->belongsTo('App\Model\vehicle', 'vehicle_id');
    }
}
