<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class customer extends Model
{
    use Notifiable;
    
    protected $table = 'customers';
    

    protected $fillable = [
        'name', 'address', 'phone_number'
    ];

    public function account() {
        return $this->hasOne('App\Model\account');
    }
    public function requirement() {
        return $this->hasMany('App\Model\requirement');
    }

}
