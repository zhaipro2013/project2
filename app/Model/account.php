<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
class account extends Authenticatable
{
    use Notifiable;
    
    protected $table = 'account';
    
    protected $guard = 'Admin';
    
    protected $fillable = [
        'username', 'password', 'auth_id', 'partner_id', 'customer_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function partner() {
        return $this->belongsTo('App\Model\partner', 'partner_id');
    }
    public function customer() {
        return $this->belongsTo('App\Model\customer', 'customer_id');
    }
    public function authority()
    {
        return $this->belongsTo('App\Model\authority', 'auth_id');
    }
}
