<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class partner extends Model
{
    use Notifiable;

    protected $table = 'partners';
    

    protected $fillable = [
        'name', 'address', 'phone_number'
    ];
    public function account() {
        return $this->hasOne('App\Model\account');
    }
}
