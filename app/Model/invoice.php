<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class invoice extends Model
{
    use Notifiable;
    protected $table = 'invoice';

    public $timestamps = false;

    protected $fillable = [
        'total', 'serviceArise', 'costIncurred', 'partner_id', 'requirement_id', 'created_at'
    ];

    public function requirement() {
        return $this->belongsTo('App\Model\requirement', 'requirement_id');
    }
}
