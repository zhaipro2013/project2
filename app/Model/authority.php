<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class authority extends Model
{
    protected $table = 'authority';

    public function account()
    {
        return $this->hasOne('App\Model\account');
    }
}
