<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class requirement extends Model
{
    public $timestamps = false;
    protected $table = "requirement";

    protected $fillable = [
        'address', 'content', 'vehicle_id', 'service_id', 'customer_id', 'partner_id', 'status', 'created_at'
    ];
    
    public function customer() {
        return $this->belongsTo('App\Model\customer', 'customer_id');
    }
    
    public function invoice() {
        return $this->hasOne('App\Model\invoice');
    }

}
