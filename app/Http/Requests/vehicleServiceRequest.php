<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class vehicleServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'serviceName' => 'required|unique:service,serviceName',
        ];
    }

    public function messages()
    {
        return [
            'serviceName.required' => 'Gặp lỗi hệ thống bạn chưa nhập tên dịch vụ',
            'serviceName.unique' => 'Tên dịch vụ đã tồn tại'
        ];
    }
}
