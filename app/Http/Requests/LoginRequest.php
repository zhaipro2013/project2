<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|min:8',
            'password' => 'required|min:6|',
        ];
    }
    public function messages()
    {
        return [
            'username.required' => 'Username không được để trống', 
            'password.required' => 'password không được để trống', 
            'password.min' => 'Password cần ít nhất 6 kí tự',
            'username.min' => 'username cần ít nhất 8 kí tự',
        ];
    }
}
