<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class registerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:32',
            'username' => 'required|unique:account,username|max:32',
            'email' => 'required|email|unique:account,email',
            'phone_number' => 'required|max:10',
            'address' => 'required',
            'password' => 'required|min:6|confirmed'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Name không được để trống',
            'username.required' => 'Username không được để trống',
            'username.unique' => 'This Username already exist!',
            'phone_number.required' => 'Phone number không được để trống',
            'address.required' => 'address không được để trống',
            'email.required' => 'Email không được để trống',
            'email.email' => 'Email không đúng định dạng',
            'email.unique' => 'This E-mail already exist!',
            'phone_number.max' => 'SĐT tối đa 10 kí tự',
            'password.min' => 'Password cần ít nhất 6 kí tự',
            'password.confirmed' => 'confirmation Password incorrect',
            'password.required' => 'Password không được để trống'
        ];
    }
}
