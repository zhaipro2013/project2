<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class vehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nameVehicle' => 'required|unique:vehicle,nameVehicle',
        ];
    }

    public function messages()
    {
        return [
            'nameVehicle.required' => 'Gặp lỗi hệ thống bạn chưa nhập tên xe',
            'nameVehicle.unique' => 'Tên xe đã tồn tại'
        ];
    }
}
