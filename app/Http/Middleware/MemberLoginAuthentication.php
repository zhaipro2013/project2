<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class MemberLoginAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if(Auth::check() && Auth::user()->authority->auth_name == $role){
            return $next($request);
        }else
            return redirect('Admin/home');
        
    }
}
