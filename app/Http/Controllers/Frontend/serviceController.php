<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\service;
use App\Model\vehicle;
use App\Model\requirement;
use App\Http\Requests\serviceRequest;
use App\Model\partner;
use App\Model\invoice;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;

class serviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showServices()
    {
        $vehicle = vehicle::all();
        $service = service::all();
        return view('Frontend.services.service', compact('vehicle', 'service'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function handleServices(serviceRequest $request)
    {
        $requirement = new requirement();
        $requirement->address = $request->address;
        $requirement->content = $request->content;
        $requirement->vehicle_id = $request->vehicle_id;
        $requirement->service_id = $request->service_id;
        $requirement->customer_id = Auth::user()->customer_id;
        $requirement->created_at = Carbon::now('Asia/Ho_Chi_Minh')->toDateTimeString();
        $requirement->save();
        return back()->with('success', 'Yêu cầu đã gữi đợi đối tác gần đó chấp nhận');
    }


    public function handleServicesAjax(Request $request)
    {
        $vehicle_id = $request->id;
        if(!empty($vehicle_id)){
            $services = service::where('vehicle_id', $request->id)->get()->toArray();
            return response($services);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showInvoice($id)
    {   $arrRequirement = [];
        $id = $this->decodeId($id);
        $requirement = $this->RequirementSession($arrRequirement, $id);
        $partner = partner::all();
        $service = service::with('vehicle')->get();
        return view('Frontend.invoice.Chitiet',compact('requirement', 'partner','service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function handleInvoice(Request $request)
    {
        if(!empty($request->requirement_id)){
            $requirement = requirement::find($request->requirement_id);
            $updateStatus['status'] = 2;
            $updateStatus['partner_id'] = $request->partner_id;;
            $requirement->update($updateStatus);
            if($requirement->update($updateStatus)){
                $invoice = new invoice();
                $invoice->total = $request->price;
                $invoice->partner_id = $request->partner_id;
                $invoice->requirement_id = $request->requirement_id;
                $invoice->created_at = Carbon::now('Asia/Ho_Chi_Minh')->toDateTimeString();
                $invoice->save();
                $flag = $this->DeleteSession(false , $request->requirement_id);
            }
            return response($flag);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function RequirementSession($arrRequirement, $id)
    {
        $requirement = [];
        //get session and rearrange array index
        if(session()->has('requirement') && !empty(session()->has('requirement'))){
            $listRequirement = array_values(session()->get('requirement'));
            $arrRequirement = requirement::where('customer_id', $id)
            ->where('status', '=', '1')
            ->get()->toArray();
            foreach ($arrRequirement as $key => $value) {
                foreach ($listRequirement as $key2 => $value2) {
                    if($value['id'] == $value2['requirement_id']){
                        $requirement[$key2] = $arrRequirement[$key];
                        $requirement[$key2]['total'] = $value2['cost'];
                        $requirement[$key2]['partner_id'] = $value2['partner_id'];
                    }
                }
            }
            array_values($requirement);
        }
        return $requirement;
    }
    public function DeleteSession($flag, $id)
    {
        //get session and rearrange array index
        if(session()->has('requirement')){
            $listRequirement = array_values(session()->get('requirement'));
                foreach ($listRequirement as $key => $value) {
                    if($value['requirement_id'] == $id){
                        unset($listRequirement[$key]);
                        $flag = true;
                    }
                }
                if ($flag) {
                    if(empty($listRequirement)){
                        session()->forget('requirement');
                    } else{
                        array_values($listRequirement);
                        session()->put('requirement', $listRequirement);
                    }
                }
            }
        return $flag;
    }
    public function decodeId($id){
        $data = Crypt::decrypt($id);
        return $data;
    }
}
