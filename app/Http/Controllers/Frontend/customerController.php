<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Requests\registerRequest;
use App\Http\Requests\LoginRequest;
use App\Model\account;
use App\Model\customer;
use App\Model\partner;
use Auth;
use DB;
use Illuminate\Support\Facades\Hash;

class customerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    use AuthenticatesUsers;
    

    public function showLogin()
    {
        return view('Frontend.customers.login');
    }


    public function Login(LoginRequest $request)
    {
        $acc = DB::table('account')->where('username', $request->username)->get()->toArray();
        if(!empty($acc)){
            $role = $acc[0]->auth_id;
        }else {
            $role = null;
        }
        
        $login = [
            'username' => $request->username,
            'password' => $request->password,
            'auth_id' => $role
        ];

        $remember = $request->has('remember_me')? true : false;


        if (Auth::attempt($login, $remember)) {
            return redirect()->route('user.service');
        } else {
            return redirect()->back()->withErrors('Username or password is incorrect.');
        }
        
    }

    public function showProfile()
    {
        return view('Frontend.customers.profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request)
    {
        $password = Auth::user()->password;
        if(empty($request->password) && empty($request->old_password)){
            $customer = customer::find(Auth::user()->customer->id);
            $customerName['name'] = $request->name;
            if($customer->update($customerName)){
                return back()->with('success','Cập nhật thành công');
            }
        }
        if(!empty($request->password) && Hash::check($request->old_password , $password)){
            $customer = customer::find(Auth::user()->customer->id);
            $customerName['name'] = $request->name;
            $account = account::find(Auth::id());
            $newPassword['password'] = bcrypt($request->password);
            if($customer->update($customerName) && $account->update($newPassword)){
                return back()->with('success','Cập nhật thành công');
            }
        } else {
            return back()->withErrors('Thao tác không đúng vui lòng nhập lại');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showRegister()
    {
        return view('Frontend.customers.register');
    }

    public function Register(registerRequest $request)
    {
        $account = new account();
        $account->username = $request->username;
        $account->password = bcrypt($request->password);
        $account->email = $request->email;
        $account->auth_id = $request->authority;
        if($request->authority == 3){
            $account->customer_id = $this->customer($request->name, $request->address, $request->phone_number);
        }
        if($request->authority == 2){
            $account->partner_id = $this->partner($request->name, $request->address, $request->phone_number);
        }
        $account->save();
        return back()->with('success', 'Đăng ký thành công');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function customer($name, $address, $phone_number)
    {
        $customer = new customer();
        $customer->name = $name;
        $customer->address = $address;
        $customer->phone_number = $phone_number;
        $customer->save();
        return $customer->id;
    }
    public function partner($name, $address, $phone_number)
    {
        $partner = new partner();
        $partner->name = $name;
        $partner->address = $address;
        $partner->phone_number = $phone_number;
        $partner->save();
        return $partner->id;
    }

    public function logout() {
        Auth::logout();
        return redirect('/member-login');
    }
}
