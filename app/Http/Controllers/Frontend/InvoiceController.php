<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Model\invoice;
use App\Model\partner;
use App\Model\service;
use App\Model\requirement;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Crypt;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showInvoiceDetail($id)
    {
        $id = $this->decodeId($id);
        /**
            * Way one.
        */
        $partner = partner::all();
        $service = service::get();
        $customer_id = $id;
        $invoice = invoice::with(['requirement' => function($query) use($customer_id){
            $query->where('customer_id', $customer_id);
        }])->get();
        /**
            * Way two.
        */
        $db = DB::table('invoice')->join('requirement','invoice.requirement_id', '=', 'requirement.id')
        ->leftJoin('service', 'requirement.service_id', '=', 'service.id')
        ->join('partners', 'requirement.partner_id', '=', 'partners.id')
        ->where('customer_id', $customer_id)->get();
        return view('Frontend.invoice.InvoiceDetail', compact('partner', 'service', 'invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function decodeId($id){
        $data = Crypt::decrypt($id);
        return $data;
    }
}
