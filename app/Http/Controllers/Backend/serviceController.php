<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\vehicleRequest;
use App\Http\Requests\vehicleServiceRequest;
use App\Model\vehicle;
use App\Model\service;
use App\Model\requirement;
use Carbon\Carbon;
use DB;
use Auth;

class serviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showVehicle()
    {
        $vehicleList = DB::table('vehicle')->get();
        return view('Backend.vehicle.vehicle',compact('vehicleList'));
    }

    public function addVehicle(vehicleRequest $request)
    {
        $vehicle = new vehicle();
        $vehicle->nameVehicle = $request->nameVehicle;
        $vehicle->save();
        return back()->with('success','Thêm thành công');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showlist()
    {
        // $now = Carbon::now('Asia/Ho_Chi_Minh');
        $list = requirement::with('customer')->get();
        $services = service::with('vehicle')->get();
        return view('Backend.service.serviceList', compact('list', 'services'));
    }

    public function handleRequirement(Request $request)
    {
        $listRequirement = [];
        $requirement = $request->all();
        $requirement['partner_id'] = Auth::user()->partner_id;
        $flag = true;
        $status = requirement::find($request->requirement_id)->select('status')->get()->toArray();
        if($status[0]['status'] == 1){
            if (session()->has('requirement')) {
                $listRequirement = session()->get('requirement');
                foreach ($listRequirement as $key => $item) {
                    if ( ($item['partner_id'] == Auth::user()->partner_id) && ($item['requirement_id'] == $request->requirement_id)) {
                        $listRequirement[$key]['cost'] = $request->cost;
                        session()->put('requirement', $listRequirement);
                        $flag = false;
                        break;
                    }
                }
                //in the case the product unavailable in session
                if ($flag) {
                    session()->push('requirement', $requirement);
                }
            } else {
                // in the case the first product add to cart
                session()->push('requirement', $requirement);
            }
            return back()->with('success', 'Đã gửi báo giá đến cho khách hàng xem');
        }else {
            return back()->withErrors('Đơn hàng đã được nhận');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showService($id)
    {
        // Carbon::setLocale('vi');
        // $now = Carbon::now('Asia/Ho_Chi_Minh');
        // $dt = Carbon::create(2021, 01, 03, 5, 21, 0, 'Asia/Ho_Chi_Minh');
        $serviceList = vehicle::where('id', $id)->with('service')->get()->toArray();
        return view('Backend.service.service',compact('serviceList'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addService(vehicleServiceRequest $request, $id)
    {
        $service = new service();
        $service->serviceName = $request->serviceName;
        $service->vehicle_id = $id;
        $service->save();
        return back()->with('success','Thêm thành công');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyVehicle($id)
    {
        vehicle::destroy($id);
        return redirect()->back()->with('success','Xóa thành công');
    }
    public function destroyService($id)
    {
        service::destroy($id);
        return redirect()->back()->with('success','Xóa thành công');
    }
}
